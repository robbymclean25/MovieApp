package com.example.movieapi.view
import androidx.fragment.app.Fragment
import android.view.View
import android.util.Log
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.movieapi.R
import com.example.movieapi.databinding.FragmentMovieBinding
import com.example.movieapi.databinding.MovieItemBinding
import com.example.movieapi.model.remote.MovieRepo
import com.example.movieapi.viewModel.MovieVMFactory
import com.example.movieapi.viewModel.MovieViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MovieFragment: Fragment() {
    val TAG = "Movie Fragment"
    lateinit var binding: FragmentMovieBinding
    lateinit var factory: MovieVMFactory
    private val viewModel by viewModels<MovieViewModel> { factory }
    private val theAdapter: MovieAdapter by lazy {
        MovieAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMovieBinding.inflate(layoutInflater)
        initObservers()
        return binding.root
    }

    override fun onViewCreated(view:View,savedInstanceState: Bundle?){

    }



    private fun initObservers() {
        viewModel.movieList.observe(viewLifecycleOwner) { state ->
            Log.e("MOVIE FRAG", state.movies.toString())
            binding.progress.isVisible = state.isLoading
            if (state.movies.isNotEmpty() && state.isLoading) {
                theAdapter.updateList(state.movies)
            }
            if (state.response == false) {
                Toast.makeText(requireContext(), "Try again", Toast.LENGTH_LONG).show()
            }
        }

    }
}









