package com.example.movieapi.view
import com.example.movieapi.R
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.os.Bundle
import android.view.View
import androidx.navigation.Navigation
import android.util.Log
import android.view.ViewGroup
import com.example.movieapi.viewModel.MovieViewModel
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapi.databinding.FragmentGenreBinding
import com.example.movieapi.viewModel.MovieVMFactory
import androidx.navigation.fragment.findNavController
import javax.inject.Inject

class GenreFragment: Fragment(R.layout.fragment_genre) {
    lateinit var binding: FragmentGenreBinding
    @Inject
    lateinit var viewModel:MovieViewModel

    private val movieAdapter by lazy{
        MovieAdapter()

    }

    override fun onCreateView(inflater: LayoutInflater,
    container:ViewGroup?,
    savedInstanceState:Bundle?):View? {
        binding = FragmentGenreBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view:View,savedInstanceState: Bundle?){
        binding.btnSearch.setOnClickListener {
            val action = GenreFragmentDirections.actionGenreFragmentToMovieFragment()
            findNavController().navigate(action)
        }
    }
    private fun setRecyclerView(){
        with(binding){

        }
    }



}