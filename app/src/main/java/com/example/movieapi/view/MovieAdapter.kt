package com.example.movieapi.view
import coil.load
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapi.databinding.MovieItemBinding
import com.example.movieapi.model.local.Movies

class MovieAdapter(): RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {
    private var list: MutableList<Movies> = mutableListOf()

    fun updateList(newList: List<Movies>){
        val oldSize = list.size
        list.clear()
        notifyItemRangeChanged(0,newList.size)
        list.addAll(newList)
        notifyItemRangeChanged(0,newList.size)
    }

    class MovieViewHolder(val binding: MovieItemBinding): RecyclerView.ViewHolder(binding.root){
        fun displayImage(movie:Movies){
           binding.ivMovieImage.load(movie.image)
        }
    }
    override fun onCreateViewHolder(parent:ViewGroup,viewType: Int):MovieViewHolder{
        return MovieViewHolder(MovieItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }
    override fun onBindViewHolder(holder: MovieViewHolder,position: Int){
        holder.displayImage(list[position])
    }
    override fun getItemCount(): Int = list.size
}