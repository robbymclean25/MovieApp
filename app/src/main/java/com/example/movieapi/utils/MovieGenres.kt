package com.example.movieapi.utils

enum class MovieGenres {
    ACTION, ROMANCE, HORROR, ADVENTURE,COMEDY
}