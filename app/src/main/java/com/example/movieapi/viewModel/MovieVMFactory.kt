package com.example.movieapi.viewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModel
import com.example.movieapi.model.remote.MovieRepo

class MovieVMFactory (
    private val repo: MovieRepo
        ):ViewModelProvider.NewInstanceFactory(){
            override fun <T: ViewModel> create(modelClass: Class<T>): T{
                return MovieViewModel(repo)as T
            }
        }
