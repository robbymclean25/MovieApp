package com.example.movieapi.viewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.movieapi.model.local.Movies
import androidx.lifecycle.ViewModel
import com.example.movieapi.model.remote.MovieRepo
import kotlinx.coroutines.launch


class MovieViewModel(private val repo: MovieRepo) : ViewModel(){
    private val _movieList: MutableLiveData<MovieState> = MutableLiveData(MovieState())
     val movieList: LiveData<MovieState> get() = _movieList

    fun getMovies(){
        viewModelScope.launch{
            _movieList.value = MovieState(isLoading = true)
            val result = repo.getMovies()
            _movieList.value = MovieState(movies = result, isLoading = false)
        }
    }
    data class MovieState(
        val isLoading: Boolean = false,
        val movies : List<Movies> = emptyList(),
        var response: Boolean? = null

    )
}