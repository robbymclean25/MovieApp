package com.example.movieapi
import dagger.hilt.android.HiltAndroidApp
import android.app.Application

@HiltAndroidApp
class Application: Application()

