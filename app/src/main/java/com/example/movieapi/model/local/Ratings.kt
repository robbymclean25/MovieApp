package com.example.movieapi.model.local

data class Ratings(
    val Source: String,
    val Value:String
)
