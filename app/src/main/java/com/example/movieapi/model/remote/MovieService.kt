package com.example.movieapi.model.remote
import android.app.appsearch.SearchResults
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.create
import okhttp3.OkHttpClient

interface MovieService {
   companion object {
       private const val BASE_URL ="https://www.omdbapi.com"
       private const val ENDPOINT =  "/"
       private const val API_KEY = "13b7c7ab"

       private fun myHttpClient(): OkHttpClient{
           val builder = OkHttpClient.Builder()
               .addInterceptor(MovieInterceptor())
           return builder.build()
       }

       fun getInstance(): MovieService = Retrofit.Builder()
           .baseUrl(BASE_URL)
           .addConverterFactory(GsonConverterFactory.create())
           .build()
           .create()
   }
    @GET(ENDPOINT)
    suspend fun getMovies(@Path("search")count:Int = 10 ): List<String>}