package com.example.movieapi.model.local
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.movieapi.utils.MovieGenres

@Dao
interface MovieDao {
    @Query("SELECT * FROM movies")
    suspend fun getAll(): List<Movies>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovie(vararg movie: Movies)

    @Query("SELECT * FROM movies WHERE genre = :movieGenres")
    suspend fun getAllTypedMovies(movieGenres: MovieGenres) : List<Movies>
}