package com.example.movieapi.model.local
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
@Database(entities = [Movies::class],version =1)
abstract class MovieDB:RoomDatabase() {
    abstract fun movieDao():MovieDao
    companion object{
        private const val DATABASE_NAME = "movies.db"
        @Volatile
        private var instance: MovieDB ?= null

        fun getInstance(context: Context):MovieDB{
            return instance?: kotlin.synchronized(this){
            instance ?:buildDatabase(context).also{
                instance = it
              }
            }
        }
        private fun buildDatabase(context:Context): MovieDB {
            return Room
                .databaseBuilder(context,MovieDB ::class.java, DATABASE_NAME)
                .build()

        }
    }

}