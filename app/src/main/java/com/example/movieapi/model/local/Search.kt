package com.example.movieapi.model.local

import com.example.movieapi.utils.MovieGenres

data class Search(
    val poster:String,
    val Title: String,
    val Genre: String,
    val Year: String,
    val imdbID: String,

){
    fun toEntitu() = Movies(
        name= Title,
        genre = MovieGenres.COMEDY,
        imdbID = imdbID,
        )
}
