package com.example.movieapi.model.remote
import android.content.Context
import com.example.movieapi.model.local.MovieDB
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import com.example.movieapi.model.local.Movies
import com.example.movieapi.utils.MovieGenres
import kotlinx.coroutines.withContext
class MovieRepo(context: Context) {
    private val movieService = MovieService.getInstance()
    private val genredao = MovieDB.getInstance(context).movieDao()
    suspend fun getMovies():List<Movies> = withContext(Dispatchers.IO ){
        val cachedMovies = genredao.getAllTypedMovies(MovieGenres.ACTION)
        delay(1000)
       return@withContext cachedMovies.ifEmpty {

           val remoteMovies = movieService.getMovies()

           //Map out list to String to a movie entity
           val entities: List<Movies> = remoteMovies.map{
               Movies(genre = MovieGenres.ACTION, image = it, name = it, search = it)
               Movies(genre = MovieGenres.ADVENTURE, image = it, name = it, search = it)
               Movies(genre = MovieGenres.COMEDY, image = it, name = it, search = it)

           }
           //Save the movies
           genredao.insertMovie(*entities.toTypedArray())
           return@ifEmpty entities
       }
    }


}