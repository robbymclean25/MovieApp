package com.example.movieapi.model.local
import androidx.room.PrimaryKey
import androidx.room.Entity
import com.example.movieapi.utils.MovieGenres

@Entity
data class Movies(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 10,
    val name: String,
    val genre: MovieGenres,
    val imdbID :String


)
