package com.example.movieapi.model.local

data class SearchResults(
    val Response: String,
    val Search: List<Search>,
    val totalResults: String,
    val Error: String?
)
